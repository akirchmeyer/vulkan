#include "input.h"
#include "settings.h"

#include <GLFW/glfw3.h>
#include <functional>
#include <memory>
#include <iostream>
#include <cassert>


struct Action1 : public Action
{
    GLFWwindow *w;

    Action1(GLFWwindow *_w) : w(_w){}
    virtual void call() {
        std::cout << "Closing\n";
        glfwSetWindowShouldClose(w, true);
    }
};

Input::Input(Settings *settings, GLFWwindow *window)
{
    settings->select ("Keys");
    auto quit1 = [&window]() {
        std::cout << "Closing\n";
        glfwSetWindowShouldClose(window, true);
    };
    Action1 quit2(window);

    ActionLambda<decltype(quit1)> lam(quit1);
    AddKeyBinding(Combination(settings->get<int>("Quit"), GLFW_PRESS, 0), &quit2);

    assert(settings->get<int>("Quit") != GLFW_KEY_UNKNOWN);
    Bind(window);
}

Input::~Input()
{

}

void HandleKeys(GLFWwindow* window, int key, int, int action, int mods)
{
    Input *input = (Input*)glfwGetWindowUserPointer(window);
    Combination event(key, action, mods);
    for(InputEvent &i : input->keys)
    {
        if(i.first == event)
        {
            std::cout << "Handling\n";
            i.second->call();
        }
    }
}

void HandleMouse(GLFWwindow* window, int button, int action, int mods)
{
    Input *input = (Input*)glfwGetWindowUserPointer(window);
    Combination event(button, action, mods);
    for(auto &i: input->mouse)
    {
        if(i.first == event)
            i.second->call();
    }
}

void Input::Bind(GLFWwindow *window)
{
    glfwSetWindowUserPointer(window, this);
    glfwSetKeyCallback(window, HandleKeys);
    glfwSetMouseButtonCallback(window, HandleMouse);
}
