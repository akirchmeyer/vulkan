#include "window.h"

#include "Renderers/vkrenderer.h"
#include "Renderers/glrenderer.h"

#include <iostream>
#include <cassert>
#include <vector>
#include <GLFW/glfw3.h>


void Window::InitGLFW()
{
    //INITIALIZE GLFW
    if (!glfwInit())
    {
        std::cout << "Error: GLFW initialization failed\n";
    }

    glfwSetErrorCallback([](int error, const char* description)
    {
        std::cerr << "GLFW Error " << error << ": " << description << '\n';
    });
}

Window::Window(const View *view, std::string const &filename) : m_settings(nullptr), m_window(nullptr), m_view(view), m_title()
{
    m_settings = new Settings();
    assert(m_settings->loadFromFile (filename));

    m_title = m_settings->getString("Window", "Title");

    //WINDOW
    InitGLFW();
    InitWindow();

    assert(m_window);

    m_input = new Input(m_settings, m_window);

    m_renderer = new VkRenderer(m_window);
    m_renderer->Init (m_settings);
}

Window::~Window()
{
    delete m_renderer;
    m_renderer = nullptr;

    delete m_input;
    m_input = nullptr;

    glfwDestroyWindow(m_window);
    m_window = nullptr;

    glfwTerminate();
}

void Window::InitWindow()
{
    m_settings->select("Window");
    uint32_t width = m_settings->get<int>("Width"), height = m_settings->get<int>("Height");

    assert(width > 0 && height > 0);

    m_window = glfwCreateWindow(width, height, m_title.c_str(), nullptr, nullptr);
    assert(m_window);

    //glfwSetWindowSizeCallback(m_window, RENDERER::Resize);
}

void Window::run()
{
    while (!glfwWindowShouldClose(m_window))
    {
        glfwPollEvents();

        if(!m_renderer->Render())
            std::cout << "Rendering is incomplete\n";
    }
}
