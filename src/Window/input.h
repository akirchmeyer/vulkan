#ifndef INPUT_H
#define INPUT_H

#include "config.h"
#include <vector>

class GLFWwindow;
class Settings;


class Combination
{
public:
    //key token: GLFW_KEY_A, ..., GLFW_KEY_UNKNOWN
    //action: GLFW_PRESS, GLFW_RELEASE, GLFW_REPEAT
    //mods: GLFW_MOD_SHIFT, GLFW_MOD_CONTROL, GLFW_MOD_ALT, GLFW_MOD_SUPER
    //scancodes: The scancode is unique for every key, regardless of whether it has a key token.
    //           Scancodes are platform-specific but consistent over time, so keys will have different scancodes depending on the platform but they are safe to save to disk.

    constexpr Combination(int k = 0, int a = 0, int m = 0) noexcept : m_key(k), m_action(a), m_mods(m) {}

    constexpr bool operator==(Combination const &b) const
    {
        return (m_key == b.m_key) && (m_action == b.m_action) && (m_mods == b.m_mods);
    }

private:
    int m_key, m_action, m_mods;
};

struct Action
{
    Action() {}
    virtual void call() {}
};

template<typename T>
struct ActionLambda : public Action
{
    ActionLambda(T const &f) : m_func(f) {}
    virtual void call() { m_func(); }

    T m_func;
};

using InputEvent = std::pair<Combination, Action*>;

class Input
{
public:
    Input(Settings *settings, GLFWwindow *window);
    ~Input();

    void AddKeyBinding(Combination const &c, Action *a)
    {
        keys.push_back(std::make_pair(c, a));
    }
    void AddMouseBinding(Combination const &c, Action *a)
    {
        mouse.push_back(std::make_pair(c, a));
    }

    std::vector<InputEvent> keys, mouse;

private:
    void Bind(GLFWwindow *window);
};

#endif // INPUT_H
