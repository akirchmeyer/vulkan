#ifndef WINDOW_H
#define WINDOW_H

#include "config.h"

#include "settings.h"
#include "input.h"

#include "memory.h"

class Renderer;
class View;

class Window
{
public:
    Window(const View *view, const std::string &filename);
    ~Window();

    void run();

    Settings *getSettings() { return m_settings; }

private:
    void InitWindow();
    void InitGLFW();

    void ResizeWindow(GLFWwindow *, int w, int h);

    Settings *m_settings;

    Input *m_input;

    GLFWwindow* m_window;
    Renderer *m_renderer;
    const View *m_view;

    std::string m_title;
};

#endif // WINDOW_H
