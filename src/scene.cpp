#include "scene.h"
#include "config.h"
#include "map.h"

#include <iostream>
#include <fstream>

Scene::Scene() : m_map(new Map())
{

}

Scene::~Scene()
{
    delete m_map;
}

void Scene::LoadMap(const char *filename)
{
    std::cout << "Loading map\n";
    std::ifstream stream(filename, std::ios_base::binary);

    assert(stream.is_open ());

    uint32 width, height;
    stream >> width >> height;

    hsize data[width*height];

    for(uint32 y = 0; y < height; ++y)
    {
        for(uint32 x = 0; x < width; ++x)
        {
            stream >> data[y * width + x];
        }
    }
    stream.close ();

    m_map->Construct (data, width, height);
}

void Scene::SaveMap(const char *filename)
{
    std::cout << "Saving map\n";
    std::ofstream stream(filename, std::ios_base::binary);

    assert(stream.is_open ());

    uint32 width = m_map->getWidth (), height = m_map->getHeight ();

    stream << width << ' ' << height << '\n';

    for(uint32 y = 0; y < height; ++y)
    {
        for(uint32 x = 0; x < width; ++x)
        {
            stream << static_cast<int>(m_map->getCell (x, y)) << ' ';
        }
        stream << '\n';
    }
    stream.close ();
}

void Scene::GenerateMap()
{
    std::cout << "Generating map\n";

    const uint32 w = 10, h = 10;

    hsize data[w*h];
    for(uint32 i = 0, n = w*h; i < n; ++i)
    {
        data[i] = 0;
    }

    m_map->Construct(data, w, h);
}

void Scene::PrintMap()
{
    m_map->Print();
}
