#ifndef MAP_H
#define MAP_H

#include "config.h"
#include <vector>
#include <cassert>

using hsize = uint8_t;

struct coord
{
    coord(uint32 _x = 0, uint32 _y = 0) : x(_x), y(_y) {}

    uint32 x, y;
};

class Map
{
public:
    Map();
    ~Map();

    inline hsize getCell(uint32 x, uint32 y) const
    {
        assert(y < m_height && x < m_width);
        return m_cells[ y*m_width + x ];
    }

    void Print() const;

    void Construct(const hsize *data, uint32 width, uint32 height);

    uint32 getWidth() const { return m_width; }
    uint32 getHeight() const { return m_height; }

private:
    uint32 m_width, m_height;
    hsize *m_cells;
};

#endif // MAP_H
