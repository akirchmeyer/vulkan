#ifndef SCENEVIEW_H
#define SCENEVIEW_H

#include "map.h"
#include <GL/glew.h>

class Scene;

class View
{
public:
    View(const Scene *scene);
    ~View();

    void SetExtent(coord m1, coord m2)
    {
        min = m1;
        max = coord(m2.x-1, m2.y-1);
        UpdateView();
    }

    void Render();

private:
    void UpdateView();

    const Scene *m_scene;
    const Map* m_map;

    coord min, max;

    GLuint index;
};

#endif // SCENEVIEW_H
