#ifndef CONFIG_H
#define CONFIG_H

#include <cstdint>

using uint32 = uint32_t;

//VULKAN
#define VULKAN_ENABLE_DEBUG
#define GLFW_INCLUDE_VULKAN

#endif // CONFIG_H
