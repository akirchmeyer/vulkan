#include "field.h"
#include <memory>
#include <iostream>

Field::Field(uint32_t width, uint32_t height) : m_data(nullptr), m_width(width), m_height(height)
{
}

Field::~Field()
{
    delete m_data;
    m_data = nullptr;
}

void Field::Construct(const uint32_t *data, uint32 width, uint32 height)
{
    m_width = width;
    m_height = height;

    m_data = new uint32_t[width*height];
    std::copy(data, data + (width*height), m_data);

    assert(m_data);

    std::cout << "Map memory size: " << sizeof(uint32_t)*width*height * 10e-9 << "Gb\n";
}
