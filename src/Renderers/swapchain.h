#ifndef SWAPCHAIN_H
#define SWAPCHAIN_H

#include "context.h"
#include "surface.h"
#include "commandpool.h"
#include <vector>

class Pipeline;

class SwapChain
{
public:
    SwapChain(uint32_t imageCount, const Context *context, const Surface *surface, uint32_t width, uint32_t height);
    ~SwapChain();

    bool Render();
    void ResizeWindow();

    uint32_t getSwapChainImageCount() const { return m_imageCount; }
    const std::vector<VkImage> &getSwapChainImages() const { return m_swapChainImages; }

    const uint32_t getWidth() const { return m_width; }
    const uint32_t getHeight() const { return m_height; }

private:
    void GetSwapChainImages();

    void AcquirePresentModes();
    void SelectSwapChainImageCount(uint32_t imageCount);

    void CreateSwapChain();
    void DestroySwapChain();

    void CreateSemaphores();
    void DestroySemaphores();

    void InitCommandBuffers();
    bool RecordCommandBuffers();
    void DestroyCommandBuffers();


    VkSwapchainKHR m_swapchain;
    VkPresentModeKHR m_presentMode = VK_PRESENT_MODE_FIFO_KHR;

    VkSemaphore m_semaphoreImageAvailable, m_semaphoreRenderingFinished;

    CommandPool *m_cmdPool;

    Pipeline *m_pipeline;

    uint32_t m_imageCount = 0;
    std::vector<VkImage> m_swapChainImages;

    const Context *m_context;
    const Surface *m_surface;

    uint32_t m_width, m_height;

    VkClearValue m_clearValue;

};

#endif // SWAPCHAIN_H
