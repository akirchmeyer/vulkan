#ifndef CONTEXT_H
#define CONTEXT_H

#include "config.h"
#include <vulkan/vulkan.h>
#include <vector>

/*typedef struct VkInstance_T* VkInstance;
typedef struct VkDevice_T* VkDevice;*/

//#define USE_PHYSICAL_PROPERTIES
//#define PRINT_INSTANCE_LAYER_PROPERTIES

void ErrorCheck( VkResult );

#include <string>

class Context
{
public:
    Context(std::string const &applicationName, bool enableSwapChain = true);
    ~Context();

    const VkDevice &GetVulkanDevice() const { return m_vkDevice; }
    const VkPhysicalDevice &GetVulkanPhysicalDevice() const { return m_vkPhysicalDevice; }
    const VkQueue &GetVulkanGraphicsQueue() const { return m_vkGraphicsQueue; }
    const VkQueue &GetVulkanPresentationQueue() const { return m_vkPresentQueue; }

    uint32_t GetVulkanPresentationFamilyIndex() const { return m_presentFamilyIndex; }
    uint32_t GetVulkanGraphicsFamilyIndex() const { return m_graphicsFamilyIndex; }

    const VkAllocationCallbacks* GetVulkanAllocationCallbacks() const { return m_vkAllocCallbacks; }
    const VkInstance &GetVulkanInstance() const { return m_vkInstance; }

    void InitDevice(const VkSurfaceKHR &surface);
    void InitPhysicalDevice();

private:
    void InitInstance(const char* applicationName);
    void DestroyInstance();

    void DestroyDevice();

    void SetupDebug();
    void InitDebug();
    void DeInitDebug();

    VkInstance m_vkInstance = VK_NULL_HANDLE;

    VkPhysicalDevice m_vkPhysicalDevice = VK_NULL_HANDLE;
#ifdef USE_PHYSICAL_PROPERTIES
    VkPhysicalDeviceProperties m_vkPhysicalDeviceProperties;
#endif
    VkDevice m_vkDevice = VK_NULL_HANDLE;

    VkQueue m_vkGraphicsQueue = VK_NULL_HANDLE, m_vkPresentQueue = VK_NULL_HANDLE;

    std::vector<const char*> m_instanceLayerList, m_instanceExtensionList, m_deviceExtensionList;
    VkDebugReportCallbackEXT m_debugReport = VK_NULL_HANDLE;
    VkDebugReportCallbackCreateInfoEXT debugCallbackCreateInfo = {};

    uint32_t m_graphicsFamilyIndex = UINT32_MAX, m_presentFamilyIndex = UINT32_MAX;

    VkAllocationCallbacks *m_vkAllocCallbacks = VK_NULL_HANDLE;
};

#endif // CONTEXT_H
