#ifndef SHADER_H
#define SHADER_H

#include "swapchain.h"

class Shader
{
public:
    Shader(const Context *context);
    ~Shader();

    bool LoadFromFile(const char* filename);
    bool LoadFromSource(const std::string &source);

    const VkShaderModule &GetShader() const { return m_shader; }

    VkPipelineShaderStageCreateInfo ConstructCreateInfo(VkPipelineShaderStageCreateFlags flags, VkShaderStageFlagBits stage, const char* name, const VkSpecializationInfo *specInfo = nullptr)
    {
        return VkPipelineShaderStageCreateInfo {
            VK_STRUCTURE_TYPE_PIPELINE_SHADER_STAGE_CREATE_INFO,        // VkStructureType                                sType
                nullptr,                                                    // const void                                    *pNext
                flags,                                                          // VkPipelineShaderStageCreateFlags               flags
                stage,                                 // VkShaderStageFlagBits                          stage
                m_shader,                                 // VkShaderModule                                 module
                name,                                                     // const char                                    *pName
                specInfo                                                     // const VkSpecializationInfo
        };
    }

private:
    const Context *m_context;
    VkShaderModule m_shader = VK_NULL_HANDLE;
};

#endif // SHADER_H
