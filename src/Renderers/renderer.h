#ifndef RENDERER_H
#define RENDERER_H

#include "config.h"
#include <string>

class Settings;

class Renderer
{
public:
    Renderer(){}
    virtual ~Renderer(){}

    virtual bool Render() = 0;
    virtual void Init(Settings *settings) = 0;
};

#endif // RENDERER_H
