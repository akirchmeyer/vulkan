#include "vkrenderer.h"
#include <vulkan/vulkan.h>
#include <GLFW/glfw3.h>

#include "settings.h"
#include "context.h"
#include "surface.h"
#include "swapchain.h"

VkRenderer::VkRenderer(GLFWwindow *win) : Renderer(), m_window(win), m_context(nullptr), m_surface(nullptr), m_swapChain(nullptr) {}

void VkRenderer::Init (Settings *settings)
{
    m_context = new Context(settings->getString ("Window", "Name"));
    m_surface = new Surface(m_context, m_window);

    m_context->InitDevice(m_surface->GetSurface());

    m_swapChain = new SwapChain(settings->getInt ("Graphics", "SwapchainCount"), m_context, m_surface, settings->getInt("Window", "Width"), settings->getInt("Window", "Height"));
}

VkRenderer::~VkRenderer()
{
    delete m_swapChain;
    m_swapChain = nullptr;
    delete m_surface;
    m_surface = nullptr;

    delete m_context;
    m_context = nullptr;
}

bool VkRenderer::Render()
{
    return m_swapChain->Render();
}

void VkRenderer::ResizeWindow(uint32_t w, uint32_t h)
{

}

void VkRenderer::Resize(GLFWwindow *win, int w, int h)
{
    VkRenderer * r = (VkRenderer*)glfwGetWindowUserPointer(win);
    r->ResizeWindow(w, h);
}
