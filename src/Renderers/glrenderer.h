#ifndef GLRENDERER_H
#define GLRENDERER_H

#include "renderer.h"

class GLFWwindow;

class GlRenderer : public Renderer
{
public:
    GlRenderer(GLFWwindow *win);
    virtual ~GlRenderer();

    bool Render() override;

    static void Resize(GLFWwindow *win, int w, int h);
    void Init(Settings *settings) override;

private:
    GLFWwindow *m_window;
};

#endif // GLRENDERER_H
