#include "surface.h"
#include <GLFW/glfw3.h>
#include <cassert>

Surface::Surface(const Context *context, GLFWwindow *window) : m_context(context)
{
    if(glfwCreateWindowSurface( m_context->GetVulkanInstance(), window, m_context->GetVulkanAllocationCallbacks(), &m_surface ) != VK_SUCCESS ) {
        glfwTerminate();
        assert( "GLFW could not create the window surface." );
    }
    AcquireCapabilitiesAndFormat();
}

Surface::~Surface()
{
    vkDestroySurfaceKHR( m_context->GetVulkanInstance(), m_surface, m_context->GetVulkanAllocationCallbacks() );
}

void Surface::AcquireCapabilitiesAndFormat()
{
    auto vkPhysicalDevice = m_context->GetVulkanPhysicalDevice();

    vkGetPhysicalDeviceSurfaceCapabilitiesKHR( vkPhysicalDevice , m_surface, &m_surfaceCapabilities );

    {
        uint32_t format_count = 0;
        vkGetPhysicalDeviceSurfaceFormatsKHR( vkPhysicalDevice, m_surface, &format_count, nullptr );
        assert( format_count > 0 && "Surface formats missing." );

        std::vector<VkSurfaceFormatKHR> formats( format_count );
        vkGetPhysicalDeviceSurfaceFormatsKHR( vkPhysicalDevice, m_surface, &format_count, formats.data() );
        if( formats[ 0 ].format == VK_FORMAT_UNDEFINED ) {
            m_surfaceFormat.format		= VK_FORMAT_B8G8R8A8_UNORM;
            m_surfaceFormat.colorSpace	= VK_COLORSPACE_SRGB_NONLINEAR_KHR;
        } else {
            m_surfaceFormat				= formats[ 0 ];
        }
    }
}
