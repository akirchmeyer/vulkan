#ifndef COMMANDPOOL_H
#define COMMANDPOOL_H

#include "context.h"

class CommandPool
{
public:
    CommandPool(const Context *context, uint32_t queueFamilyIndex, uint32_t size);
    ~CommandPool();

    const VkCommandPool& GetCmdPool() const { return m_pool; }
    const std::vector<VkCommandBuffer>& GetCmdBuffers() const { return m_buffers; }

private:
    bool CreateCommandPool();
    bool AllocateCommandPool();

    VkCommandPool m_pool = VK_NULL_HANDLE;
    std::vector<VkCommandBuffer> m_buffers;

    const Context *m_context;
    uint32_t m_queueFamilyIndex;
};

#endif // COMMANDPOOL_H
