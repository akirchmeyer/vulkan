#include "commandpool.h"
#include <cassert>

CommandPool::CommandPool(const Context *context, uint32_t queueFamilyIndex, uint32_t size) : m_context(context), m_queueFamilyIndex(queueFamilyIndex)
{
    m_buffers.resize(size, VK_NULL_HANDLE);

    assert(CreateCommandPool());
    assert(AllocateCommandPool());
}

CommandPool::~CommandPool()
{
    vkDeviceWaitIdle( m_context->GetVulkanDevice() );
    if( (m_buffers.size() > 0) && (m_buffers[0] != VK_NULL_HANDLE) ) {
        vkFreeCommandBuffers( m_context->GetVulkanDevice(), m_pool, static_cast<uint32_t>(m_buffers.size()), m_buffers.data() );
        m_buffers.clear();
    }

    if( m_pool != VK_NULL_HANDLE ) {
        vkDestroyCommandPool( m_context->GetVulkanDevice(), m_pool, m_context->GetVulkanAllocationCallbacks() );
        m_pool = VK_NULL_HANDLE;
    }
}

bool CommandPool::CreateCommandPool()
{
    VkCommandPoolCreateInfo cmd_pool_create_info = {
        VK_STRUCTURE_TYPE_COMMAND_POOL_CREATE_INFO,     // VkStructureType                sType
        nullptr,                                        // const void                    *pNext
        0,                                              // VkCommandPoolCreateFlags       flags
        m_queueFamilyIndex                              // uint32_t                       queueFamilyIndex
    };

    if( vkCreateCommandPool( m_context->GetVulkanDevice(), &cmd_pool_create_info, m_context->GetVulkanAllocationCallbacks(), &m_pool ) != VK_SUCCESS ) {
        m_pool = VK_NULL_HANDLE;
        return false;
    }
    return true;
}


bool CommandPool::AllocateCommandPool()
{
    VkCommandBufferAllocateInfo command_buffer_allocate_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_ALLOCATE_INFO, // VkStructureType                sType
      nullptr,                                        // const void                    *pNext
      m_pool,                                           // VkCommandPool                  commandPool
      VK_COMMAND_BUFFER_LEVEL_PRIMARY,                // VkCommandBufferLevel           level
      static_cast<uint32_t>(m_buffers.size())                                           // uint32_t                       bufferCount
    };

    if( vkAllocateCommandBuffers( m_context->GetVulkanDevice(), &command_buffer_allocate_info, m_buffers.data() ) != VK_SUCCESS ) {
      return false;
    }
    return true;
}
