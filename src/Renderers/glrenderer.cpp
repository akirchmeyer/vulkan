#include "glrenderer.h"
#include <GL/glew.h>
#include <GLFW/glfw3.h>
#include <iostream>
#include "view.h"
#include "INI.h"

GlRenderer::GlRenderer(GLFWwindow *win) : Renderer(), m_window(win)
{
    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 2);
    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
}

GlRenderer::~GlRenderer()
{

}

bool GlRenderer::Render()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT );
    glClearColor(1.f, 0.f, 0.f, 1.f);

    glfwSwapBuffers(m_window);

    return true;
}

void GlRenderer::Resize(GLFWwindow *win, int w, int h)
{
    glfwSetWindowSize(win, w, h);
    glViewport(0, 0, w, h);
}

void GlRenderer::Init(Settings *settings)
{
    glfwMakeContextCurrent(m_window);
    glfwSwapInterval(1);

    glewExperimental=GL_TRUE;

    GLenum res = glewInit();

    if (res != GLEW_OK)
        std::cout << "GLEW error: " << glewGetErrorString(res) << '\n';
}
