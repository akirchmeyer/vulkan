#include "pipeline.h"
#include "swapchain.h"

#include "config.h"
#include <vulkan/vulkan.h>

#include <cassert>
#include <iostream>

#include "shader.h"

Pipeline::Pipeline(const Context *context, const Surface *surface, const SwapChain *swapChain) : m_context(context)
{
    Init(surface, swapChain);
}

Pipeline::~Pipeline()
{
    vkDeviceWaitIdle( m_context->GetVulkanDevice() );

    vkDestroyPipeline(m_context->GetVulkanDevice(), m_graphicsPipeline, m_context->GetVulkanAllocationCallbacks());
    vkDestroyRenderPass(m_context->GetVulkanDevice(), m_renderPass, m_context->GetVulkanAllocationCallbacks());

    for( size_t i = 0, n = m_frameBuffers.size(); i < n; ++i ) {
        if( m_frameBuffers[i] != VK_NULL_HANDLE ) {
            vkDestroyFramebuffer( m_context->GetVulkanDevice(), m_frameBuffers[i], m_context->GetVulkanAllocationCallbacks() );
            m_frameBuffers[i] = VK_NULL_HANDLE;
        }

        if( m_imageViews[i] != VK_NULL_HANDLE ) {
            vkDestroyImageView( m_context->GetVulkanDevice(), m_imageViews[i], m_context->GetVulkanAllocationCallbacks() );
            m_imageViews[i] = VK_NULL_HANDLE;
        }
    }
    m_frameBuffers.clear();
    m_imageViews.clear();
}

void Pipeline::Init(const Surface *surface, const SwapChain *swapChain)
{
    assert(CreateRenderPass(surface->GetSurfaceFormat().format));
    assert(CreateImageViewsAndFrameBuffers(surface->GetSurfaceFormat().format, swapChain, swapChain->getWidth(), swapChain->getHeight()));
    assert(CreatePipeline(swapChain->getWidth(), swapChain->getHeight()));
}

bool Pipeline::CreateRenderPass(VkFormat const &format)
{
    //RENDERPASS ATTACHMENT DESCRIPTION
    VkAttachmentDescription attachment_descriptions[] = {
        {
            0,                                   // VkAttachmentDescriptionFlags   flags
            format,               // VkFormat                       format
            VK_SAMPLE_COUNT_1_BIT,               // VkSampleCountFlagBits          samples
            VK_ATTACHMENT_LOAD_OP_CLEAR,         // VkAttachmentLoadOp             loadOp
            VK_ATTACHMENT_STORE_OP_STORE,        // VkAttachmentStoreOp            storeOp
            VK_ATTACHMENT_LOAD_OP_DONT_CARE,     // VkAttachmentLoadOp             stencilLoadOp
            VK_ATTACHMENT_STORE_OP_DONT_CARE,    // VkAttachmentStoreOp            stencilStoreOp
            VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,     // VkImageLayout                  initialLayout;
            VK_IMAGE_LAYOUT_PRESENT_SRC_KHR      // VkImageLayout                  finalLayout
        }
    };

    //SUBPASS DESCRIPTION
    VkAttachmentReference color_attachment_references[] = {
        {
            0,                                          // uint32_t                       attachment
            VK_IMAGE_LAYOUT_COLOR_ATTACHMENT_OPTIMAL    // VkImageLayout                  layout
        }
    };

    VkSubpassDescription subpass_descriptions[] = {
        {
            0,                                          // VkSubpassDescriptionFlags      flags
            VK_PIPELINE_BIND_POINT_GRAPHICS,            // VkPipelineBindPoint            pipelineBindPoint
            0,                                          // uint32_t                       inputAttachmentCount
            nullptr,                                    // const VkAttachmentReference   *pInputAttachments
            1,                                          // uint32_t                       colorAttachmentCount
            color_attachment_references,                // const VkAttachmentReference   *pColorAttachments
            nullptr,                                    // const VkAttachmentReference   *pResolveAttachments
            nullptr,                                    // const VkAttachmentReference   *pDepthStencilAttachment
            0,                                          // uint32_t                       preserveAttachmentCount
            nullptr                                     // const uint32_t*                pPreserveAttachments
        }
    };

    //RENDERPASS CREATION
    VkRenderPassCreateInfo render_pass_create_info = {
        VK_STRUCTURE_TYPE_RENDER_PASS_CREATE_INFO,    // VkStructureType                sType
        nullptr,                                      // const void                    *pNext
        0,                                            // VkRenderPassCreateFlags        flags
        1,                                            // uint32_t                       attachmentCount
        attachment_descriptions,                      // const VkAttachmentDescription *pAttachments
        1,                                            // uint32_t                       subpassCount
        subpass_descriptions,                         // const VkSubpassDescription    *pSubpasses
        0,                                            // uint32_t                       dependencyCount
        nullptr                                       // const VkSubpassDependency     *pDependencies
    };


    if( vkCreateRenderPass( m_context->GetVulkanDevice(), &render_pass_create_info, m_context->GetVulkanAllocationCallbacks(), &m_renderPass ) != VK_SUCCESS ) {
        printf( "Could not create render pass!\n" );
        m_renderPass = VK_NULL_HANDLE;
        return false;
    }

    return true;
}

bool Pipeline::CreateImageViewsAndFrameBuffers(VkFormat const &format, const SwapChain *swapChain, uint32_t width, uint32_t height)
{
    const std::vector<VkImage> &swap_chain_images = swapChain->getSwapChainImages();

    assert(swap_chain_images.size() > 0);
    m_frameBuffers.resize( swap_chain_images.size() );
    m_imageViews.resize( swap_chain_images.size() );

    for( size_t i = 0; i < swap_chain_images.size(); ++i ) {
        VkImageViewCreateInfo image_view_create_info = {
            VK_STRUCTURE_TYPE_IMAGE_VIEW_CREATE_INFO,   // VkStructureType                sType
            nullptr,                                    // const void                    *pNext
            0,                                          // VkImageViewCreateFlags         flags
            swap_chain_images[i],                       // VkImage                        image
            VK_IMAGE_VIEW_TYPE_2D,                      // VkImageViewType                viewType
            format,                      // VkFormat                       format
            {                                           // VkComponentMapping             components
                VK_COMPONENT_SWIZZLE_IDENTITY,              // VkComponentSwizzle             r
                VK_COMPONENT_SWIZZLE_IDENTITY,              // VkComponentSwizzle             g
                VK_COMPONENT_SWIZZLE_IDENTITY,              // VkComponentSwizzle             b
                VK_COMPONENT_SWIZZLE_IDENTITY               // VkComponentSwizzle             a
            },
            {                                           // VkImageSubresourceRange        subresourceRange
                VK_IMAGE_ASPECT_COLOR_BIT,                  // VkImageAspectFlags             aspectMask
                0,                                          // uint32_t                       baseMipLevel
                1,                                          // uint32_t                       levelCount
                0,                                          // uint32_t                       baseArrayLayer
                1                                           // uint32_t                       layerCount
            }
        };

        if( vkCreateImageView( m_context->GetVulkanDevice(), &image_view_create_info, m_context->GetVulkanAllocationCallbacks(), &m_imageViews[i] ) != VK_SUCCESS ) {
          std::cout <<   "Could not create image view for framebuffer!\n" ;
          return false;
        }

        VkFramebufferCreateInfo framebuffer_create_info = {
            VK_STRUCTURE_TYPE_FRAMEBUFFER_CREATE_INFO,  // VkStructureType                sType
            nullptr,                                    // const void                    *pNext
            0,                                          // VkFramebufferCreateFlags       flags
            m_renderPass,                          // VkRenderPass                   renderPass
            1,                                          // uint32_t                       attachmentCount
            &m_imageViews[i],    // const VkImageView             *pAttachmentsm_f
            width,                                        // uint32_t                       width
            height,                                        // uint32_t                       height
            1                                           // uint32_t                       layers
        };

        if( vkCreateFramebuffer( m_context->GetVulkanDevice(), &framebuffer_create_info, m_context->GetVulkanAllocationCallbacks(), &m_frameBuffers[i] ) != VK_SUCCESS ) {
            std::cout <<  "Could not create a framebuffer!\n" ;
            return false;
        }
    }
    return true;
}

bool Pipeline::CreatePipeline(uint32_t width, uint32_t height)
{

    //SHADERS
    Shader vert(m_context), frag(m_context);
    assert(vert.LoadFromFile("vert.spv") && frag.LoadFromFile("frag.spv"));

    std::vector<VkPipelineShaderStageCreateInfo> shader_stage_create_infos = {
      // Vertex shader
        vert.ConstructCreateInfo(0, VK_SHADER_STAGE_VERTEX_BIT, "main"),
      // Fragment shader
        frag.ConstructCreateInfo(0, VK_SHADER_STAGE_FRAGMENT_BIT, "main")
    };

    VkPipelineVertexInputStateCreateInfo vertex_input_state_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_VERTEX_INPUT_STATE_CREATE_INFO,    // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineVertexInputStateCreateFlags          flags;
      0,                                                            // uint32_t                                       vertexBindingDescriptionCount
      nullptr,                                                      // const VkVertexInputBindingDescription         *pVertexBindingDescriptions
      0,                                                            // uint32_t                                       vertexAttributeDescriptionCount
      nullptr                                                       // const VkVertexInputAttributeDescription       *pVertexAttributeDescriptions
    };

    //VIEWPORT

    VkPipelineInputAssemblyStateCreateInfo input_assembly_state_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_INPUT_ASSEMBLY_STATE_CREATE_INFO,  // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineInputAssemblyStateCreateFlags        flags
      VK_PRIMITIVE_TOPOLOGY_TRIANGLE_LIST,                          // VkPrimitiveTopology                            topology
      VK_FALSE                                                      // VkBool32                                       primitiveRestartEnable
    };

    VkViewport viewport = {
      0.0f,                                                         // float                                          x
      0.0f,                                                         // float                                          y
      static_cast<float>(width),                                                       // float                                          width
      static_cast<float>(height),                                                       // float                                          height
      0.0f,                                                         // float                                          minDepth
      1.0f                                                          // float                                          maxDepth
    };

    VkRect2D scissor = {
      {                                                             // VkOffset2D                                     offset
        0,                                                            // int32_t                                        x
        0                                                             // int32_t                                        y
      },
      {                                                             // VkExtent2D                                     extent
        width,                                                          // int32_t                                        width
        height                                                           // int32_t                                        height
      }
    };

    VkPipelineViewportStateCreateInfo viewport_state_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_VIEWPORT_STATE_CREATE_INFO,        // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineViewportStateCreateFlags             flags
      1,                                                            // uint32_t                                       viewportCount
      &viewport,                                                    // const VkViewport                              *pViewports
      1,                                                            // uint32_t                                       scissorCount
      &scissor                                                      // const VkRect2D                                *pScissors
    };

    //RASTERIZATION

    VkPipelineRasterizationStateCreateInfo rasterization_state_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_RASTERIZATION_STATE_CREATE_INFO,   // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineRasterizationStateCreateFlags        flags
      VK_FALSE,                                                     // VkBool32                                       depthClampEnable
      VK_FALSE,                                                     // VkBool32                                       rasterizerDiscardEnable
      VK_POLYGON_MODE_FILL,                                         // VkPolygonMode                                  polygonMode
      VK_CULL_MODE_BACK_BIT,                                        // VkCullModeFlags                                cullMode
      VK_FRONT_FACE_COUNTER_CLOCKWISE,                              // VkFrontFace                                    frontFace
      VK_FALSE,                                                     // VkBool32                                       depthBiasEnable
      0.0f,                                                         // float                                          depthBiasConstantFactor
      0.0f,                                                         // float                                          depthBiasClamp
      0.0f,                                                         // float                                          depthBiasSlopeFactor
      1.0f                                                          // float                                          lineWidth
    };

    //MULTISAMPLING

    VkPipelineMultisampleStateCreateInfo multisample_state_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_MULTISAMPLE_STATE_CREATE_INFO,     // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineMultisampleStateCreateFlags          flags
      VK_SAMPLE_COUNT_1_BIT,                                        // VkSampleCountFlagBits                          rasterizationSamples
      VK_FALSE,                                                     // VkBool32                                       sampleShadingEnable
      1.0f,                                                         // float                                          minSampleShading
      nullptr,                                                      // const VkSampleMask                            *pSampleMask
      VK_FALSE,                                                     // VkBool32                                       alphaToCoverageEnable
      VK_FALSE                                                      // VkBool32                                       alphaToOneEnable
    };

    //BLENDING STATE

    VkPipelineColorBlendAttachmentState color_blend_attachment_state = {
      VK_FALSE,                                                     // VkBool32                                       blendEnable
      VK_BLEND_FACTOR_ONE,                                          // VkBlendFactor                                  srcColorBlendFactor
      VK_BLEND_FACTOR_ZERO,                                         // VkBlendFactor                                  dstColorBlendFactor
      VK_BLEND_OP_ADD,                                              // VkBlendOp                                      colorBlendOp
      VK_BLEND_FACTOR_ONE,                                          // VkBlendFactor                                  srcAlphaBlendFactor
      VK_BLEND_FACTOR_ZERO,                                         // VkBlendFactor                                  dstAlphaBlendFactor
      VK_BLEND_OP_ADD,                                              // VkBlendOp                                      alphaBlendOp
      VK_COLOR_COMPONENT_R_BIT | VK_COLOR_COMPONENT_G_BIT |         // VkColorComponentFlags                          colorWriteMask
      VK_COLOR_COMPONENT_B_BIT | VK_COLOR_COMPONENT_A_BIT
    };

    VkPipelineColorBlendStateCreateInfo color_blend_state_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_COLOR_BLEND_STATE_CREATE_INFO,     // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineColorBlendStateCreateFlags           flags
      VK_FALSE,                                                     // VkBool32                                       logicOpEnable
      VK_LOGIC_OP_COPY,                                             // VkLogicOp                                      logicOp
      1,                                                            // uint32_t                                       attachmentCount
      &color_blend_attachment_state,                                // const VkPipelineColorBlendAttachmentState     *pAttachments
      { 0.0f, 0.0f, 0.0f, 0.0f }                                    // float                                          blendConstants[4]
    };

    PipelineLayout pipeline_layout(m_context);
    if( !pipeline_layout.CreatePipelineLayout() ) {
      return false;
    }

    VkGraphicsPipelineCreateInfo pipeline_create_info = {
      VK_STRUCTURE_TYPE_GRAPHICS_PIPELINE_CREATE_INFO,              // VkStructureType                                sType
      nullptr,                                                      // const void                                    *pNext
      0,                                                            // VkPipelineCreateFlags                          flags
      static_cast<uint32_t>(shader_stage_create_infos.size()),      // uint32_t                                       stageCount
      &shader_stage_create_infos[0],                                // const VkPipelineShaderStageCreateInfo         *pStages
      &vertex_input_state_create_info,                              // const VkPipelineVertexInputStateCreateInfo    *pVertexInputState;
      &input_assembly_state_create_info,                            // const VkPipelineInputAssemblyStateCreateInfo  *pInputAssemblyState
      nullptr,                                                      // const VkPipelineTessellationStateCreateInfo   *pTessellationState
      &viewport_state_create_info,                                  // const VkPipelineViewportStateCreateInfo       *pViewportState
      &rasterization_state_create_info,                             // const VkPipelineRasterizationStateCreateInfo  *pRasterizationState
      &multisample_state_create_info,                               // const VkPipelineMultisampleStateCreateInfo    *pMultisampleState
      nullptr,                                                      // const VkPipelineDepthStencilStateCreateInfo   *pDepthStencilState
      &color_blend_state_create_info,                               // const VkPipelineColorBlendStateCreateInfo     *pColorBlendState
      nullptr,                                                      // const VkPipelineDynamicStateCreateInfo        *pDynamicState
      pipeline_layout.getPipelineLayout(),                                        // VkPipelineLayout                               layout
      m_renderPass,                                            // VkRenderPass                                   renderPass
      0,                                                            // uint32_t                                       subpass
      VK_NULL_HANDLE,                                               // VkPipeline                                     basePipelineHandle
      -1                                                            // int32_t                                        basePipelineIndex
    };

    if( vkCreateGraphicsPipelines( m_context->GetVulkanDevice(), VK_NULL_HANDLE, 1, &pipeline_create_info, nullptr, &m_graphicsPipeline ) != VK_SUCCESS ) {
      std::cout << "Could not create graphics pipeline!\n";
      return false;
    }

    return true;
}

PipelineLayout::PipelineLayout(const Context *context): m_context(context)
{
}

bool PipelineLayout::CreatePipelineLayout()
{
    VkPipelineLayoutCreateInfo layout_create_info = {
      VK_STRUCTURE_TYPE_PIPELINE_LAYOUT_CREATE_INFO,  // VkStructureType                sType
      nullptr,                                        // const void                    *pNext
      0,                                              // VkPipelineLayoutCreateFlags    flags
      0,                                              // uint32_t                       setLayoutCount
      nullptr,                                        // const VkDescriptorSetLayout   *pSetLayouts
      0,                                              // uint32_t                       pushConstantRangeCount
      nullptr                                         // const VkPushConstantRange     *pPushConstantRanges
    };
    if( vkCreatePipelineLayout( m_context->GetVulkanDevice(), &layout_create_info, nullptr, &m_layout ) != VK_SUCCESS ) {
      std::cout << "Could not create pipeline layout!\n";
      m_layout = VK_NULL_HANDLE;
      return false;
    }

    return true;
}

PipelineLayout::~PipelineLayout()
{
    if(m_layout != VK_NULL_HANDLE)
        vkDestroyPipelineLayout(m_context->GetVulkanDevice(), m_layout, m_context->GetVulkanAllocationCallbacks());
}
