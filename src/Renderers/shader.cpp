#include "shader.h"
#include <fstream>
#include <iostream>

template<class T, class F>
class AutoDeleter {
public:
  AutoDeleter() :
    Object( VK_NULL_HANDLE ),
    Deleter( nullptr ),
    Device( VK_NULL_HANDLE ) {
  }

  AutoDeleter( T object, F deleter, VkDevice device ) :
    Object( object ),
    Deleter( deleter ),
    Device( device ) {
  }

  AutoDeleter( AutoDeleter&& other ) {
    *this = std::move( other );
  }

  ~AutoDeleter() {
    if( (Object != VK_NULL_HANDLE) && (Deleter != nullptr) && (Device != VK_NULL_HANDLE) ) {
      Deleter( Device, Object, nullptr );
    }
  }

  AutoDeleter& operator=(AutoDeleter&& other) {
    if( this != &other ) {
      Object = other.Object;
      Deleter = other.Deleter;
      Device = other.Device;
      other.Object = VK_NULL_HANDLE;
    }
    return *this;
  }

  T Get() {
    return Object;
  }

  bool operator !() const {
    return Object == VK_NULL_HANDLE;
  }

private:
  AutoDeleter( const AutoDeleter& );
  AutoDeleter& operator=( const AutoDeleter& );
  T         Object;
  F         Deleter;
  VkDevice  Device;
};

std::vector<char> GetBinaryFileContents( std::string const &filename ) {

  std::ifstream file( filename, std::ios::binary );
  if( file.fail() ) {
    std::cout << "Could not open \"" << filename << "\" file!" << std::endl;
    return std::vector<char>();
  }

  std::streampos begin, end;
  begin = file.tellg();
  file.seekg( 0, std::ios::end );
  end = file.tellg();

  std::vector<char> result( static_cast<size_t>(end - begin) );
  file.seekg( 0, std::ios::beg );
  file.read( &result[0], end - begin );
  file.close();

  return result;
}


Shader::Shader(const Context *context) : m_context(context)
{
}

Shader::~Shader()
{
    if(m_shader != VK_NULL_HANDLE)
        vkDestroyShaderModule(m_context->GetVulkanDevice(), m_shader, m_context->GetVulkanAllocationCallbacks());
}

bool Shader::LoadFromSource(const std::string &source)
{
    if(source.size() == 0) return false;

    VkShaderModuleCreateInfo shader_module_create_info = {
        VK_STRUCTURE_TYPE_SHADER_MODULE_CREATE_INFO,    // VkStructureType                sType
        nullptr,                                        // const void                    *pNext
        0,                                              // VkShaderModuleCreateFlags      flags
        source.size(),                                    // size_t                         codeSize
        reinterpret_cast<const uint32_t*>(source.c_str())     // const uint32_t                *pCode
    };

    if( vkCreateShaderModule( m_context->GetVulkanDevice(), &shader_module_create_info, nullptr, &m_shader ) != VK_SUCCESS ) {
        std::cout <<  "Could not create shader module\n";
        m_shader = VK_NULL_HANDLE;
        return false;
    }
    return true;
}

bool Shader::LoadFromFile(const char *filename)
{
    const std::vector<char> code = GetBinaryFileContents( filename );
    return LoadFromSource(std::string(code.begin(), code.end()));
}
