#ifndef SURFACE_H
#define SURFACE_H

#include "context.h"

class GLFWwindow;

class Surface
{
public:
    Surface(const Context *context, GLFWwindow *window);
    ~Surface();

    const VkSurfaceKHR &GetSurface() const { return m_surface; }
    const VkSurfaceFormatKHR& GetSurfaceFormat() const { return m_surfaceFormat; }
    const VkSurfaceCapabilitiesKHR& GetSurfaceCapabilities() const { return m_surfaceCapabilities; }

private:
    void AcquireCapabilitiesAndFormat();

    const Context *m_context;
    VkSurfaceKHR m_surface = VK_NULL_HANDLE;
    VkSurfaceFormatKHR m_surfaceFormat = {};
    VkSurfaceCapabilitiesKHR m_surfaceCapabilities = {};
};

#endif // SURFACE_H
