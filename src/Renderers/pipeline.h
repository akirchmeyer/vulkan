#ifndef PIPELINE_H
#define PIPELINE_H

#include "context.h"
#include <vector>

class Surface;
class SwapChain;

class PipelineLayout
{
public:
    PipelineLayout(const Context *context);
    ~PipelineLayout();
    bool CreatePipelineLayout();

    const VkPipelineLayout& getPipelineLayout() const { return m_layout; }


private:
    const Context *m_context;
    VkPipelineLayout m_layout = VK_NULL_HANDLE;
};

class Pipeline
{
public:
    Pipeline(const Context *context, const Surface *surface, const SwapChain *swapChain);
    ~Pipeline();

    const VkRenderPass& getRenderPass() const { return m_renderPass; }
    const VkPipeline& getGraphicsPipeline() const { return m_graphicsPipeline; }
    const std::vector<VkFramebuffer>& getFrameBuffers() const { return m_frameBuffers; }
    const std::vector<VkImageView>& getImageViews() const { return m_imageViews; }

private:
    void Init(const Surface *surface, const SwapChain *swapChain);
    bool CreateRenderPass(const VkFormat &format);
    bool CreateImageViewsAndFrameBuffers(VkFormat const &format, const SwapChain *swapChain, uint32_t width, uint32_t height);
    bool CreatePipeline(uint32_t width, uint32_t height);

    VkRenderPass m_renderPass;
    VkPipeline m_graphicsPipeline;
    std::vector<VkFramebuffer> m_frameBuffers;
    std::vector<VkImageView> m_imageViews;

    const Context *m_context;
};

#endif // PIPELINE_H
