#include "swapchain.h"
#include "pipeline.h"
#include <cmath>
#include <cassert>
#include <iostream>

SwapChain::SwapChain(uint32_t imageCount, const Context *context, const Surface *surface, uint32_t width, uint32_t height) : m_context(context), m_surface(surface), m_width(width), m_height(height)
{
    const auto & vkDevice = m_context->GetVulkanDevice();

    SelectSwapChainImageCount(imageCount);
    AcquirePresentModes();

    CreateSwapChain();
    ErrorCheck(vkGetSwapchainImagesKHR(vkDevice, m_swapchain, &m_imageCount, nullptr));

    CreateSemaphores();

    InitCommandBuffers();

    m_clearValue = {
        { 1.0f, 0.8f, 0.4f, 0.0f },                     // VkClearColorValue              color
    };
}


void SwapChain::CreateSwapChain()
{
    //const auto & surfaceCapabilities = m_surface->GetSurfaceCapabilities();
    const auto & surfaceFormat = m_surface->GetSurfaceFormat();
    const auto & vkDevice = m_context->GetVulkanDevice();
    const auto & vkAllocCallbacks = m_context->GetVulkanAllocationCallbacks();
    const auto & vkSurface = m_surface->GetSurface();

    VkSwapchainCreateInfoKHR swapchainCreateInfo {};
    swapchainCreateInfo.sType = VK_STRUCTURE_TYPE_SWAPCHAIN_CREATE_INFO_KHR;
    swapchainCreateInfo.surface = vkSurface;
    swapchainCreateInfo.minImageCount = m_imageCount; // double buffering
    swapchainCreateInfo.imageFormat = surfaceFormat.format;
    swapchainCreateInfo.imageColorSpace = surfaceFormat.colorSpace;

    swapchainCreateInfo.imageExtent.width = m_width;
    swapchainCreateInfo.imageExtent.height = m_height; //surfaceCapabilities.currentExtent;

    swapchainCreateInfo.imageArrayLayers = 1;
    swapchainCreateInfo.imageUsage = VK_IMAGE_USAGE_COLOR_ATTACHMENT_BIT;
    swapchainCreateInfo.imageSharingMode = VK_SHARING_MODE_EXCLUSIVE;
    swapchainCreateInfo.queueFamilyIndexCount = 0;
    swapchainCreateInfo.pQueueFamilyIndices = nullptr;
    swapchainCreateInfo.preTransform = VK_SURFACE_TRANSFORM_IDENTITY_BIT_KHR;
    swapchainCreateInfo.compositeAlpha = VK_COMPOSITE_ALPHA_OPAQUE_BIT_KHR;
    swapchainCreateInfo.presentMode = m_presentMode;
    swapchainCreateInfo.clipped = VK_TRUE;
    swapchainCreateInfo.oldSwapchain = VK_NULL_HANDLE;

    ErrorCheck(vkCreateSwapchainKHR(vkDevice, &swapchainCreateInfo, vkAllocCallbacks, &m_swapchain));
}

void SwapChain::DestroySwapChain()
{
    vkDestroySwapchainKHR(m_context->GetVulkanDevice(), m_swapchain, m_context->GetVulkanAllocationCallbacks());
}

void SwapChain::SelectSwapChainImageCount(uint32_t imageCount)
{
    const auto & surfaceCapabilities = m_surface->GetSurfaceCapabilities();
    m_imageCount = std::max(std::min(imageCount, surfaceCapabilities.maxImageCount), surfaceCapabilities.minImageCount+1);
}

void SwapChain::CreateSemaphores()
{
    const auto & vkDevice = m_context->GetVulkanDevice();

    VkSemaphoreCreateInfo semaphore_create_info {};
    semaphore_create_info.sType = VK_STRUCTURE_TYPE_SEMAPHORE_CREATE_INFO;
    semaphore_create_info.flags = 0;

    ErrorCheck(vkCreateSemaphore( vkDevice, &semaphore_create_info, m_context->GetVulkanAllocationCallbacks(), &m_semaphoreImageAvailable ));
    ErrorCheck(vkCreateSemaphore( vkDevice, &semaphore_create_info, m_context->GetVulkanAllocationCallbacks(), &m_semaphoreRenderingFinished ));
}

void SwapChain::DestroySemaphores()
{
    const auto & vkDevice = m_context->GetVulkanDevice();
    vkDestroySemaphore(vkDevice, m_semaphoreImageAvailable, m_context->GetVulkanAllocationCallbacks());
    vkDestroySemaphore(vkDevice, m_semaphoreRenderingFinished, m_context->GetVulkanAllocationCallbacks());
}

void SwapChain::AcquirePresentModes()
{
    const auto & vkSurface = m_surface->GetSurface();
    const auto & vkPhysicalDevice = m_context->GetVulkanPhysicalDevice();

    m_presentMode = VK_PRESENT_MODE_FIFO_KHR;
    {
        uint32_t count = 0;
        ErrorCheck(vkGetPhysicalDeviceSurfacePresentModesKHR(vkPhysicalDevice, vkSurface, &count, nullptr));
        std::vector<VkPresentModeKHR> presentModes(count);
        ErrorCheck(vkGetPhysicalDeviceSurfacePresentModesKHR(vkPhysicalDevice, vkSurface, &count, presentModes.data()));

        for(auto &i: presentModes)
        {
            if(i == VK_PRESENT_MODE_MAILBOX_KHR)
            {
                m_presentMode = i;
                break;
            }
        }
    }
}

SwapChain::~SwapChain()
{
    m_swapChainImages.clear();

    DestroyCommandBuffers();
    DestroySemaphores();
    DestroySwapChain();
}

bool SwapChain::Render()
{
    uint32_t image_index;

    VkResult result = vkAcquireNextImageKHR( m_context->GetVulkanDevice(), m_swapchain, UINT64_MAX, m_semaphoreImageAvailable, VK_NULL_HANDLE, &image_index );
    switch( result ) {
      case VK_SUCCESS:
      case VK_SUBOPTIMAL_KHR:
        break;
      case VK_ERROR_OUT_OF_DATE_KHR:
        ResizeWindow(); return true;
      default:
        printf( "Problem occurred during swap chain image acquisition!\n" );
        return false;
    }

    VkPipelineStageFlags wait_dst_stage_mask = VK_PIPELINE_STAGE_TRANSFER_BIT;

    VkSubmitInfo submit_info = {
      VK_STRUCTURE_TYPE_SUBMIT_INFO,                // VkStructureType              sType
      nullptr,                                      // const void                  *pNext
      1,                                            // uint32_t                     waitSemaphoreCount
      &m_semaphoreImageAvailable,                   // const VkSemaphore           *pWaitSemaphores
      &wait_dst_stage_mask,                         // const VkPipelineStageFlags  *pWaitDstStageMask;
      1,                                            // uint32_t                     commandBufferCount
      &m_cmdPool->GetCmdBuffers()[image_index],  // const VkCommandBuffer       *pCommandBuffers
      1,                                            // uint32_t                     signalSemaphoreCount
      &m_semaphoreRenderingFinished                 // const VkSemaphore           *pSignalSemaphores
    };

    if( vkQueueSubmit( m_context->GetVulkanGraphicsQueue(), 1, &submit_info, VK_NULL_HANDLE ) != VK_SUCCESS ) {
        std::cout << "Error submit\n";
      return false;
    }

    VkPresentInfoKHR present_info = {
      VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,           // VkStructureType              sType
      nullptr,                                      // const void                  *pNext
      1,                                            // uint32_t                     waitSemaphoreCount
      &m_semaphoreRenderingFinished,                // const VkSemaphore           *pWaitSemaphores
      1,                                            // uint32_t                     swapchainCount
      &m_swapchain,                                  // const VkSwapchainKHR        *pSwapchains
      &image_index,                                 // const uint32_t              *pImageIndices
      nullptr                                       // VkResult                    *pResults
    };
    result = vkQueuePresentKHR( m_context->GetVulkanPresentationQueue(), &present_info );

    switch( result ) {
      case VK_SUCCESS:
        break;
      case VK_ERROR_OUT_OF_DATE_KHR:
      case VK_SUBOPTIMAL_KHR:
        ResizeWindow(); return true;
      default:
        printf( "Problem occurred during image presentation!\n" );
        return false;
    }

    return true;

    /*
    //ACQUIRE IMAGE
    uint32_t image_index;
    VkResult result = vkAcquireNextImageKHR( m_context->GetVulkanDevice(), m_swapchain, UINT64_MAX, m_semaphoreImageAvailable, VK_NULL_HANDLE, &image_index) ;

    switch( result ) {
    case VK_SUCCESS:
    case VK_SUBOPTIMAL_KHR:
        break;
    case VK_ERROR_OUT_OF_DATE_KHR:
        ResizeWindow();
        return true;
    default:
        return false;
    }

    //TELL QUEUE TO WAIT
    VkPipelineStageFlags wait_dst_stage_mask = VK_PIPELINE_STAGE_TRANSFER_BIT;

    VkSubmitInfo submit_info = {
      VK_STRUCTURE_TYPE_SUBMIT_INFO,                // VkStructureType              sType
      nullptr,                                      // const void                  *pNext
      1,                                            // uint32_t                     waitSemaphoreCount
      &m_semaphoreImageAvailable,                   // const VkSemaphore           *pWaitSemaphores
      &wait_dst_stage_mask,                         // const VkPipelineStageFlags  *pWaitDstStageMask;
      1,                                            // uint32_t                     commandBufferCount
      &m_cmdPool->GetCmdBuffers()[image_index],                 // const VkCommandBuffer       *pCommandBuffers
      1,                                            // uint32_t                     signalSemaphoreCount
      &m_semaphoreRenderingFinished                 // const VkSemaphore           *pSignalSemaphores
    };

    if( vkQueueSubmit( m_context->GetVulkanPresentationQueue(), 1, &submit_info, VK_NULL_HANDLE ) != VK_SUCCESS ) {
      return false;
    }

    //DISPLAY

    VkPresentInfoKHR present_info = {
      VK_STRUCTURE_TYPE_PRESENT_INFO_KHR,           // VkStructureType              sType
      nullptr,                                      // const void                  *pNext
      1,                                            // uint32_t                     waitSemaphoreCount
      &m_semaphoreRenderingFinished,           // const VkSemaphore           *pWaitSemaphores
      1,                                            // uint32_t                     swapchainCount
      &m_swapchain,                            // const VkSwapchainKHR        *pSwapchains
      &image_index,                                 // const uint32_t              *pImageIndices
      nullptr                                       // VkResult                    *pResults
    };

    result = vkQueuePresentKHR( m_context->GetVulkanPresentationQueue(), &present_info );

    switch( result ) {
      case VK_SUCCESS:
        break;
      case VK_ERROR_OUT_OF_DATE_KHR:
      case VK_SUBOPTIMAL_KHR:
        ResizeWindow();
        return true;
      default:
        return false;
    }

    return true;*/
}

void SwapChain::InitCommandBuffers()
{
    GetSwapChainImages();

    m_pipeline = new Pipeline(m_context, m_surface, this);

    m_cmdPool = new CommandPool(m_context, m_context->GetVulkanPresentationFamilyIndex(), m_imageCount);

    assert(RecordCommandBuffers() && "Vulkan Error: unable to record swap chain command buffers");
}

bool SwapChain::RecordCommandBuffers()
{
    VkCommandBufferBeginInfo graphics_command_buffer_begin_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,    // VkStructureType                        sType
      nullptr,                                        // const void                            *pNext
      VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT,   // VkCommandBufferUsageFlags              flags
      nullptr                                         // const VkCommandBufferInheritanceInfo  *pInheritanceInfo
    };

    VkImageSubresourceRange image_subresource_range = {
      VK_IMAGE_ASPECT_COLOR_BIT,                      // VkImageAspectFlags             aspectMask
      0,                                              // uint32_t                       baseMipLevel
      1,                                              // uint32_t                       levelCount
      0,                                              // uint32_t                       baseArrayLayer
      1                                               // uint32_t                       layerCount
    };

    const std::vector<VkCommandBuffer> &buffers = m_cmdPool->GetCmdBuffers();

    bool callBarrier = m_context->GetVulkanGraphicsQueue() != m_context->GetVulkanPresentationQueue();
    auto renderPass = m_pipeline->getRenderPass();
    auto frameBuffers = m_pipeline->getFrameBuffers();
    assert(renderPass != VK_NULL_HANDLE && frameBuffers.size() > 0);

    for( size_t i = 0, n = buffers.size(); i < n; ++i ) {
      assert(vkBeginCommandBuffer( buffers[i], &graphics_command_buffer_begin_info ) == VK_SUCCESS);

      if( callBarrier ) {
        VkImageMemoryBarrier barrier_from_present_to_draw = {
          VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,     // VkStructureType                sType
          nullptr,                                    // const void                    *pNext
          VK_ACCESS_MEMORY_READ_BIT,                  // VkAccessFlags                  srcAccessMask
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,       // VkAccessFlags                  dstAccessMask
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,            // VkImageLayout                  oldLayout
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,            // VkImageLayout                  newLayout
          m_context->GetVulkanPresentationFamilyIndex(),              // uint32_t                       srcQueueFamilyIndex
          m_context->GetVulkanGraphicsFamilyIndex(),             // uint32_t                       dstQueueFamilyIndex
          m_swapChainImages[i],                       // VkImage                        image
          image_subresource_range                     // VkImageSubresourceRange        subresourceRange
        };
        vkCmdPipelineBarrier( buffers[i], VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier_from_present_to_draw );
      }

      VkRenderPassBeginInfo render_pass_begin_info = {
        VK_STRUCTURE_TYPE_RENDER_PASS_BEGIN_INFO,     // VkStructureType                sType
        nullptr,                                      // const void                    *pNext
        renderPass,                            // VkRenderPass                   renderPass
        frameBuffers[i],          // VkFramebuffer                  framebuffer
        {                                             // VkRect2D                       renderArea
          {                                           // VkOffset2D                     offset
            0,                                          // int32_t                        x
            0                                           // int32_t                        y
          },
          {                                           // VkExtent2D                     extent
            m_width,                                        // int32_t                        width
            m_height,                                        // int32_t                        height
          }
        },
        1,                                            // uint32_t                       clearValueCount
        &m_clearValue                                  // const VkClearValue            *pClearValues
      };

      vkCmdBeginRenderPass( buffers[i], &render_pass_begin_info, VK_SUBPASS_CONTENTS_INLINE );

      vkCmdBindPipeline( buffers[i], VK_PIPELINE_BIND_POINT_GRAPHICS, m_pipeline->getGraphicsPipeline() );

      vkCmdDraw( buffers[i], 3, 1, 0, 0 );

      vkCmdEndRenderPass( buffers[i] );

      if( callBarrier ) {
        VkImageMemoryBarrier barrier_from_draw_to_present = {
          VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,       // VkStructureType              sType
          nullptr,                                      // const void                  *pNext
          VK_ACCESS_COLOR_ATTACHMENT_WRITE_BIT,         // VkAccessFlags                srcAccessMask
          VK_ACCESS_MEMORY_READ_BIT,                    // VkAccessFlags                dstAccessMask
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,              // VkImageLayout                oldLayout
          VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,              // VkImageLayout                newLayout
          m_context->GetVulkanGraphicsFamilyIndex(),               // uint32_t                     srcQueueFamilyIndex
          m_context->GetVulkanPresentationFamilyIndex(),               // uint32_t                     dstQueueFamilyIndex
          m_swapChainImages[i],                         // VkImage                      image
          image_subresource_range                       // VkImageSubresourceRange      subresourceRange
        };
        vkCmdPipelineBarrier( buffers[i], VK_PIPELINE_STAGE_COLOR_ATTACHMENT_OUTPUT_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier_from_draw_to_present );
      }

      if( vkEndCommandBuffer( buffers[i] ) != VK_SUCCESS ) {
        std::cout << "Could not record command buffer!\n" ;
        return false;
      }
    }
    return true;

}

/*
bool SwapChain::RecordCommandBuffers()
{
    //INITIAL VARIABLES

    auto presentQueueFamilyIndex = m_context->GetVulkanPresentationFamilyIndex();

    VkCommandBufferBeginInfo cmd_buffer_begin_info = {
      VK_STRUCTURE_TYPE_COMMAND_BUFFER_BEGIN_INFO,  // VkStructureType                        sType
      nullptr,                                      // const void                            *pNext
      VK_COMMAND_BUFFER_USAGE_SIMULTANEOUS_USE_BIT, // VkCommandBufferUsageFlags              flags
      nullptr                                       // const VkCommandBufferInheritanceInfo  *pInheritanceInfo
    };

    VkClearColorValue clear_color = {
      { 1.0f, 0.8f, 0.4f, 0.0f }
    };


    VkImageSubresourceRange image_subresource_range = {
      VK_IMAGE_ASPECT_COLOR_BIT,                    // VkImageAspectFlags                     aspectMask
      0,                                            // uint32_t                               baseMipLevel
      1,                                            // uint32_t                               levelCount
      0,                                            // uint32_t                               baseArrayLayer
      1                                             // uint32_t                               layerCount
    };



    for( uint32_t i = 0; i < m_imageCount; ++i ) {
      VkImageMemoryBarrier barrier_from_present_to_clear = {
        VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,     // VkStructureType                        sType
        nullptr,                                    // const void                            *pNext
        VK_ACCESS_MEMORY_READ_BIT,                  // VkAccessFlags                          srcAccessMask
        VK_ACCESS_TRANSFER_WRITE_BIT,               // VkAccessFlags                          dstAccessMask
        VK_IMAGE_LAYOUT_UNDEFINED,                  // VkImageLayout                          oldLayout
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,       // VkImageLayout                          newLayout
        presentQueueFamilyIndex,             // uint32_t                               srcQueueFamilyIndex
        presentQueueFamilyIndex,             // uint32_t                               dstQueueFamilyIndex
        m_swapChainImages[i],                       // VkImage                                image
        image_subresource_range                     // VkImageSubresourceRange                subresourceRange
      };

      VkImageMemoryBarrier barrier_from_clear_to_present = {
        VK_STRUCTURE_TYPE_IMAGE_MEMORY_BARRIER,     // VkStructureType                        sType
        nullptr,                                    // const void                            *pNext
        VK_ACCESS_TRANSFER_WRITE_BIT,               // VkAccessFlags                          srcAccessMask
        VK_ACCESS_MEMORY_READ_BIT,                  // VkAccessFlags                          dstAccessMask
        VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL,       // VkImageLayout                          oldLayout
        VK_IMAGE_LAYOUT_PRESENT_SRC_KHR,            // VkImageLayout                          newLayout
        presentQueueFamilyIndex,             // uint32_t                               srcQueueFamilyIndex
        presentQueueFamilyIndex,             // uint32_t                               dstQueueFamilyIndex
        m_swapChainImages[i],                       // VkImage                                image
        image_subresource_range                     // VkImageSubresourceRange                subresourceRange
      };

      vkBeginCommandBuffer( m_presentQueueCmdBuffers[i], &cmd_buffer_begin_info );
      vkCmdPipelineBarrier( m_presentQueueCmdBuffers[i], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_TRANSFER_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier_from_present_to_clear );

      vkCmdClearColorImage( m_presentQueueCmdBuffers[i], m_swapChainImages[i], VK_IMAGE_LAYOUT_TRANSFER_DST_OPTIMAL, &clear_color, 1, &image_subresource_range );

      vkCmdPipelineBarrier( m_presentQueueCmdBuffers[i], VK_PIPELINE_STAGE_TRANSFER_BIT, VK_PIPELINE_STAGE_BOTTOM_OF_PIPE_BIT, 0, 0, nullptr, 0, nullptr, 1, &barrier_from_clear_to_present );
      if( vkEndCommandBuffer( m_presentQueueCmdBuffers[i] ) != VK_SUCCESS ) {
        std::cout << "Could not record command buffers!" << std::endl;
        return false;
      }
    }

    return true;
}
*/
void SwapChain::ResizeWindow()
{
    InitCommandBuffers();
}

void SwapChain::GetSwapChainImages()
{
    m_imageCount = 0;
    ErrorCheck( vkGetSwapchainImagesKHR( m_context->GetVulkanDevice(), m_swapchain, &m_imageCount, nullptr ));
    assert(m_imageCount > 0 && "Vulkan Error: Unable to get number of swapchain images" );

    m_swapChainImages.resize( m_imageCount );
    assert( vkGetSwapchainImagesKHR( m_context->GetVulkanDevice(), m_swapchain, &m_imageCount, m_swapChainImages.data() ) == VK_SUCCESS );
}

void SwapChain::DestroyCommandBuffers()
{
    if(m_cmdPool) delete m_cmdPool;
    m_cmdPool = nullptr;
    if(m_pipeline) delete m_pipeline;
    m_pipeline = nullptr;
}
