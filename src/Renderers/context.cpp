#include "context.h"

#include <cassert>
#include <vector>

#ifdef VULKAN_ENABLE_DEBUG
  #include <iostream>
  #include <iomanip>

  #ifdef _WIN32
    #include <windows.h>
  #endif
#endif

#include <GLFW/glfw3.h>


#ifdef VULKAN_ENABLE_DEBUG

void ErrorCheck( VkResult result )
{
    if( result < 0 ) {
        switch( result ) {
        case VK_ERROR_OUT_OF_HOST_MEMORY:
            std::cout << "VK_ERROR_OUT_OF_HOST_MEMORY" << std::endl;
            break;
        case VK_ERROR_OUT_OF_DEVICE_MEMORY:
            std::cout << "VK_ERROR_OUT_OF_DEVICE_MEMORY" << std::endl;
            break;
        case VK_ERROR_INITIALIZATION_FAILED:
            std::cout << "VK_ERROR_INITIALIZATION_FAILED" << std::endl;
            break;
        case VK_ERROR_DEVICE_LOST:
            std::cout << "VK_ERROR_DEVICE_LOST" << std::endl;
            break;
        case VK_ERROR_MEMORY_MAP_FAILED:
            std::cout << "VK_ERROR_MEMORY_MAP_FAILED" << std::endl;
            break;
        case VK_ERROR_LAYER_NOT_PRESENT:
            std::cout << "VK_ERROR_LAYER_NOT_PRESENT" << std::endl;
            break;
        case VK_ERROR_EXTENSION_NOT_PRESENT:
            std::cout << "VK_ERROR_EXTENSION_NOT_PRESENT" << std::endl;
            break;
        case VK_ERROR_FEATURE_NOT_PRESENT:
            std::cout << "VK_ERROR_FEATURE_NOT_PRESENT" << std::endl;
            break;
        case VK_ERROR_INCOMPATIBLE_DRIVER:
            std::cout << "VK_ERROR_INCOMPATIBLE_DRIVER" << std::endl;
            break;
        case VK_ERROR_TOO_MANY_OBJECTS:
            std::cout << "VK_ERROR_TOO_MANY_OBJECTS" << std::endl;
            break;
        case VK_ERROR_FORMAT_NOT_SUPPORTED:
            std::cout << "VK_ERROR_FORMAT_NOT_SUPPORTED" << std::endl;
            break;
        case VK_ERROR_SURFACE_LOST_KHR:
            std::cout << "VK_ERROR_SURFACE_LOST_KHR" << std::endl;
            break;
        case VK_ERROR_NATIVE_WINDOW_IN_USE_KHR:
            std::cout << "VK_ERROR_NATIVE_WINDOW_IN_USE_KHR" << std::endl;
            break;
        case VK_SUBOPTIMAL_KHR:
            std::cout << "VK_SUBOPTIMAL_KHR" << std::endl;
            break;
        case VK_ERROR_OUT_OF_DATE_KHR:
            std::cout << "VK_ERROR_OUT_OF_DATE_KHR" << std::endl;
            break;
        case VK_ERROR_INCOMPATIBLE_DISPLAY_KHR:
            std::cout << "VK_ERROR_INCOMPATIBLE_DISPLAY_KHR" << std::endl;
            break;
        case VK_ERROR_VALIDATION_FAILED_EXT:
            std::cout << "VK_ERROR_VALIDATION_FAILED_EXT" << std::endl;
            break;
        default:
            break;
        }
        assert( 0 && "Vulkan runtime error." );
    }
}

#else

void ErrorCheck( VkResult ) {}

#endif // BUILD_ENABLE_VULKAN_RUNTIME_DEBUG

Context::Context(const std::string &applicationName, bool enableSwapChain)
{
    if(enableSwapChain)
    {
        m_deviceExtensionList.push_back(VK_KHR_SWAPCHAIN_EXTENSION_NAME);

        uint32_t extCount = 0;
        const char ** extBuffer = glfwGetRequiredInstanceExtensions( &extCount );
        for( uint32_t i=0; i < extCount; ++i ) {
            // Push back required instance extensions as well
            m_instanceExtensionList.push_back( extBuffer[ i ] );
        }
    }

    SetupDebug();

    InitInstance(applicationName.c_str());
    InitDebug();

    InitPhysicalDevice();
}

Context::~Context()
{
    DestroyDevice();
    DeInitDebug();
    DestroyInstance();
}

void Context::InitInstance(const char *applicationName)
//INSTANCE CREATION
{
    //APPLICATION INFO
    VkApplicationInfo applicationInfo {};
    applicationInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
    applicationInfo.apiVersion = VK_MAKE_VERSION(1,0,17);
    applicationInfo.applicationVersion = VK_MAKE_VERSION(0, 1, 0);
    applicationInfo.pApplicationName = applicationName;

    //INSTANCE CREATION INFO
    VkInstanceCreateInfo instanceCreationInfo {};
    instanceCreationInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
    instanceCreationInfo.pApplicationInfo = &applicationInfo;

    instanceCreationInfo.enabledLayerCount = m_instanceLayerList.size();
    instanceCreationInfo.ppEnabledLayerNames = m_instanceLayerList.data();

    instanceCreationInfo.enabledExtensionCount = m_instanceExtensionList.size();
    instanceCreationInfo.ppEnabledExtensionNames = m_instanceExtensionList.data();

    instanceCreationInfo.pNext = &debugCallbackCreateInfo;

    assert(vkCreateInstance(&instanceCreationInfo, m_vkAllocCallbacks, &m_vkInstance) == VK_SUCCESS && "Vulkan Error: Instance creation failed");
}

void Context::DestroyInstance()
{
    vkDestroyInstance(m_vkInstance, m_vkAllocCallbacks);
    m_vkInstance = VK_NULL_HANDLE;
}

void Context::InitPhysicalDevice()
{
    //GET PHYSICAL DEVICES COUNT
    uint32_t deviceCount = 0;
    ErrorCheck(vkEnumeratePhysicalDevices(m_vkInstance, &deviceCount, nullptr));
    assert(deviceCount > 0 && "Vulkan Error: No Physical Device (GPUs, ...) found");

    //GET FIRST PHYSICAL DEVICE
    std::vector<VkPhysicalDevice> physicalDevices(deviceCount);
    ErrorCheck(vkEnumeratePhysicalDevices(m_vkInstance, &deviceCount, physicalDevices.data()));

    m_vkPhysicalDevice = physicalDevices[0];

#ifdef USE_PHYSICAL_PROPERTIES
        //GET PHYSICAL DEVICE PROPERTIES
        ErrorCheck(vkGetPhysicalDeviceProperties(m_vkPhysicalDevice, &m_vkPhysicalDeviceProperties));
#endif
}

void Context::InitDevice(const VkSurfaceKHR &surface)
//DEVICE CREATION
{
#ifdef PRINT_INSTANCE_LAYER_PROPERTIES
    //INSTANCE LAYER PROPERTIES
    {
        uint32_t layerCount = 0;
        ErrorCheck(vkEnumerateInstanceLayerProperties(&layerCount, nullptr));
        assert(layerCount > 0 && "Vulkan Error: No Instance Layer found");
        std::vector<VkLayerProperties> layerProperties(layerCount);
        ErrorCheck(vkEnumerateInstanceLayerProperties(&layerCount, layerProperties.data()));

        std::cout << "Instance Layers\n";
        for(auto& i : layerProperties)
        {
            std::cout << "  " << i.layerName  << " \t\t\t | " << i.description << '\n';
        }
    }
#endif


    //PRIORITIES
    std::array<float, 1> queuePriorities = {1.f};

    //QUEUE CREATION INFO
    std::vector<VkDeviceQueueCreateInfo> queueCreateInfo;

    //GET QUEUE FAMILY INDEX
    {
        uint32_t queueFamilyCount = 0;
        vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &queueFamilyCount, nullptr);
        std::vector<VkQueueFamilyProperties> queueFamilyProperties(queueFamilyCount);
        vkGetPhysicalDeviceQueueFamilyProperties(m_vkPhysicalDevice, &queueFamilyCount, queueFamilyProperties.data());
        std::vector<VkBool32> queuePresentSupport(queueFamilyCount);

        for(uint32_t i = 0; i < queueFamilyCount; ++i)
        {
            vkGetPhysicalDeviceSurfaceSupportKHR(m_vkPhysicalDevice, i, surface, &queuePresentSupport[i]);

            if(queueFamilyProperties[i].queueFlags & VK_QUEUE_GRAPHICS_BIT)
            {
                if(m_graphicsFamilyIndex == UINT32_MAX)
                    m_graphicsFamilyIndex = i;

                if(queuePresentSupport[i] == VK_TRUE)
                {
                    m_graphicsFamilyIndex = i;
                    m_presentFamilyIndex = i;
                    break;
                }
            }
            else if(queuePresentSupport[i] == VK_TRUE)
                m_presentFamilyIndex = i;
        }

        assert( m_graphicsFamilyIndex != UINT32_MAX && m_presentFamilyIndex != UINT32_MAX && "Vulkan Error: No Queue Family supporting graphics or presenting found");


        VkDeviceQueueCreateInfo queue1{};
        queue1.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
        queue1.queueCount = queuePriorities.size();
        queue1.pQueuePriorities = queuePriorities.data();
        queue1.queueFamilyIndex = m_graphicsFamilyIndex;
        queueCreateInfo.push_back(queue1);

        if(m_graphicsFamilyIndex != m_presentFamilyIndex)
        {
            VkDeviceQueueCreateInfo queue2{};
            queue2.sType = VK_STRUCTURE_TYPE_DEVICE_QUEUE_CREATE_INFO;
            queue2.queueCount = queuePriorities.size();
            queue2.pQueuePriorities = queuePriorities.data();
            queue2.queueFamilyIndex = m_presentFamilyIndex;
            queueCreateInfo.push_back(queue2);
        }
    }
    VkPhysicalDeviceFeatures features {};
    features.shaderClipDistance = VK_TRUE;


    //DEVICE CREATION INFO
    VkDeviceCreateInfo deviceInfo {};
    deviceInfo.sType = VK_STRUCTURE_TYPE_DEVICE_CREATE_INFO;
    deviceInfo.pEnabledFeatures = &features;
    deviceInfo.queueCreateInfoCount = queueCreateInfo.size();
    deviceInfo.pQueueCreateInfos = queueCreateInfo.data();
    deviceInfo.enabledExtensionCount = m_deviceExtensionList.size();
    deviceInfo.ppEnabledExtensionNames = m_deviceExtensionList.data();

    assert(vkCreateDevice(m_vkPhysicalDevice, &deviceInfo, m_vkAllocCallbacks, &m_vkDevice) == VK_SUCCESS && "Vulkan Error: Device creation failed");

    vkGetDeviceQueue(m_vkDevice, m_graphicsFamilyIndex, 0, &m_vkGraphicsQueue);
    vkGetDeviceQueue(m_vkDevice, m_presentFamilyIndex, 0, &m_vkPresentQueue);
}

void Context::DestroyDevice()
{
    vkDestroyDevice(m_vkDevice, m_vkAllocCallbacks);
    m_vkDevice = VK_NULL_HANDLE;
}

#ifdef VULKAN_ENABLE_DEBUG

VKAPI_ATTR VkBool32 VKAPI_CALL VulkanDebugCallback(
        VkDebugReportFlagsEXT                       flags,
        VkDebugReportObjectTypeEXT                  objectType,
        uint64_t                                    object,
        size_t                                      location,
        int32_t                                     messageCode,
        const char*                                 pLayerPrefix,
        const char*                                 pMessage,
        void*                                       pUserData)
{
    std::string type;

    switch (flags)
    {
    case VK_DEBUG_REPORT_INFORMATION_BIT_EXT: type = "INFO"; return true; //break;
    case VK_DEBUG_REPORT_WARNING_BIT_EXT: type = "WARNING"; break;
    case VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT: type = "PERF"; break;
    case VK_DEBUG_REPORT_ERROR_BIT_EXT: type = "ERROR"; break;
    case VK_DEBUG_REPORT_DEBUG_BIT_EXT: type = "DEBUG"; break;
    default: type = "UNKNOWN";
    };

    std::ostringstream stream;
    stream << std::left << std::setw(7) << type << std::setw(14) << pLayerPrefix << std::setw(3) << messageCode << pMessage << '\n';
    std::cout << stream.str();

#ifdef _WIN32
    if(flags & VK_DEBUG_REPORT_ERROR_BIT_EXT)
    {
        MessageBox(NULL, stream.str().c_str(), "Vulkan Error", 0);
    }
#endif

    return false;
}

void Context::SetupDebug()
{
    m_instanceLayerList.push_back("VK_LAYER_LUNARG_standard_validation");
    m_instanceExtensionList.push_back(VK_EXT_DEBUG_REPORT_EXTENSION_NAME);

    debugCallbackCreateInfo.sType = VK_STRUCTURE_TYPE_DEBUG_REPORT_CREATE_INFO_EXT;
    debugCallbackCreateInfo.pfnCallback = VulkanDebugCallback;
    debugCallbackCreateInfo.flags =
            VK_DEBUG_REPORT_INFORMATION_BIT_EXT |
            VK_DEBUG_REPORT_WARNING_BIT_EXT |
            VK_DEBUG_REPORT_PERFORMANCE_WARNING_BIT_EXT |
            VK_DEBUG_REPORT_ERROR_BIT_EXT |
            VK_DEBUG_REPORT_DEBUG_BIT_EXT |
            0;
}

PFN_vkCreateDebugReportCallbackEXT fvkCreateDebugReportCallbackEXT = nullptr;
PFN_vkDestroyDebugReportCallbackEXT fvkDestroyDebugReportCallbackEXT = nullptr;

void Context::InitDebug()
{
    fvkCreateDebugReportCallbackEXT = reinterpret_cast<PFN_vkCreateDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_vkInstance, "vkCreateDebugReportCallbackEXT"));
    fvkDestroyDebugReportCallbackEXT = reinterpret_cast<PFN_vkDestroyDebugReportCallbackEXT>(vkGetInstanceProcAddr(m_vkInstance, "vkDestroyDebugReportCallbackEXT"));
    assert(fvkCreateDebugReportCallbackEXT != nullptr && fvkDestroyDebugReportCallbackEXT != nullptr && "Vulkan Error: Cannot fetch debug function pointers");

    fvkCreateDebugReportCallbackEXT(m_vkInstance, &debugCallbackCreateInfo, m_vkAllocCallbacks, &m_debugReport);
}

void Context::DeInitDebug()
{
    fvkDestroyDebugReportCallbackEXT(m_vkInstance, m_debugReport, m_vkAllocCallbacks);
    m_debugReport = VK_NULL_HANDLE;
}

#else
void Context::SetupDebug(){}
void Context::InitDebug(){}
void Context::DeInitDebug(){}
#endif
