#ifndef VKRENDERER_H
#define VKRENDERER_H

#include "renderer.h"

class GLFWwindow;

class Context;
class Surface;
class SwapChain;

class VkRenderer : public Renderer
{
public:
    VkRenderer(GLFWwindow *win);
    virtual ~VkRenderer();

    void Init(Settings *settings) override;

    bool Render() override;

    void ResizeWindow(uint32_t w, uint32_t h);

    static void Resize(GLFWwindow *win, int w, int h);

private:
    GLFWwindow *m_window;
    Context* m_context;
    Surface *m_surface;
    SwapChain *m_swapChain;
};

#endif // VKRENDERER_H
