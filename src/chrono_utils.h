#ifndef CHRONO_UTILS_H
#define CHRONO_UTILS_H

#include <chrono>

namespace utils
{

using ns = std::chrono::nanoseconds;
using us = std::chrono::microseconds;
using ms = std::chrono::milliseconds;
using sec = std::chrono::seconds;

using fus = std::chrono::duration<float, std::micro>;
using fms = std::chrono::duration<float, std::milli>;

using clock = std::chrono::system_clock;


struct timer
{
    clock::time_point epoch;
    timer() : epoch(clock::now()) {}

    void start() { epoch = clock::now(); }

    clock::duration getEpoch() const
    {
        return clock::now() - epoch;
    }
};
}


#endif // CHRONO_UTILS_H
