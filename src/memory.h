#ifndef MEMORY_H
#define MEMORY_H

#include <memory>

template<typename T> using uptr = std::unique_ptr<T>;

#endif // MEMORY_H
