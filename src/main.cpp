#include <iostream>
#include "Window/window.h"
#include "scene.h"
#include "view.h"
#include <cassert>
#include <iomanip>

int main()
{
    {
        std::vector<int> map;
        int w = 0, h = 0;

        while(true)
        {
            std::string cmd;
            std::cout << "Command List:\nQ = Quit\nG = Generate new Map\n";
            std::cin >> cmd;
            if(cmd == "Q")
                break;
            else if(cmd == "G")
            {
                std::cout << "Enter size: ";
                std::cin >> w >> h;

                map.resize (w*h);

                for(int y = 0; y < h; ++y)
                {
                    for(int x = 0; x < w; ++x)
                    {
                        map[y*w + x] = 0;
                    }
                }
            }
            else if(cmd == "P")
            {
                std::cout << "Printing Map: \n";

                for(int y = 0; y < h; ++y)
                {
                    for(int x = 0; x < w; ++x)
                    {
                        std::cout << std::setw(2) << (int)map[y*w + x] << ' ';
                    }
                    std::cout << '\n';
                }
                std::cout << '\n';
            }
            else if(cmd == "A")
            {
                int v;
                std::cin >> v;
                int x1, x2, y1, y2;
                std::cin >> x1 >> x2 >> y1 >> y2;
                for(; y1 <= y2; ++y1)
                {
                    for(; x1 <= x2; ++x1)
                    {
                        map[y1*w+x1] = v;
                    }
                }
            }
        }

        /*
        Scene scene;
        scene.LoadMap("map.txt");
        View view(&scene);

        Window window(&view, std::string("settings.ini") );
        window.run();*/
    }

    return 0;
}
