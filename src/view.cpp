#include "view.h"
#include "scene.h"

View::View(const Scene *scene) : m_scene(scene), m_map(scene->GetMap())
{

}

View::~View()
{
}

void View::Render()
{
    for(uint32_t y = min.y; y < max.y; ++y)
    {
        for(uint32_t x = min.x; x < max.x; ++x)
        {
            hsize col = m_map->getCell(x, y);
            glBegin(GL_TRIANGLE_STRIP);
            glColor3ub(col, col, col);
            glVertex2i(x, y);
            glVertex2i(x, y+1);
            glVertex2i(x+1, y);
            glVertex2i(x+1, y+1);
            glEnd();
        }
    }
}

void View::UpdateView()
{
    /*for(uint32_t y = min.y; y < max.y; ++y)
    {
        for(uint32_t x = min.x; x < max.x; ++x)
        {

        }
    }*/
}
