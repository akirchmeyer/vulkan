#ifndef FIELD_H
#define FIELD_H

#include "config.h"
#include <stdint.h>
#include <cassert>

class Field
{
public:
    Field(uint32_t width, uint32_t height);
    ~Field();

    inline uint32_t getCell(uint32 x, uint32 y) const
    {
        assert(y < m_height && x < m_width);
        return m_data[ y*m_width + x ];
    }

    void setCell(uint32 x, uint32 y, uint32_t v)
    {
        assert(y < m_height && x < m_width);
        m_data[ y*m_width + x ] = v;
    }

    void Construct(const uint32_t *data, uint32 width, uint32 height);


private:
    uint32_t *m_data;
    uint32_t m_width, m_height;

};

#endif // FIELD_H
