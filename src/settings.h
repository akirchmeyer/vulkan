#ifndef SETTINGS_H
#define SETTINGS_H

#include <string>
#include "INI.h"

class Settings
{
public:
    Settings();
    ~Settings();

    bool loadFromFile(const std::string &filename);
    bool loadFromMemory(void *data, size_t size);

    float getFloat(std::string const &section, std::string const &key);
    int getInt(std::string const &section, std::string const &key);
    std::string getString(std::string const &section, std::string const &key);

    void select(std::string const &str)
    {
        stream.select(str);
    }

    template<typename T>T get(std::string const &key)
    {
        return stream.get (key, T());
    }

private:
    INI_t stream;
};

#endif // SETTINGS_H
