#include "settings.h"

Settings::Settings() : stream()
{

}

Settings::~Settings ()
{
}

bool Settings::loadFromFile (const std::string &filename)
{
    return stream.loadFromFile (filename);
}

bool Settings::loadFromMemory(void *data, size_t size)
{
    return stream.loadFromMemory (data, size);
}

int Settings::getInt (const std::string &section, const std::string &key)
{
    return stream.get(section, key, 0);
}

float Settings::getFloat (const std::string &section, const std::string &key)
{
    return stream.get(section, key, 0.f);
}

std::string Settings::getString (const std::string &section, const std::string &key)
{
    static const std::string strNull = "";
    return stream.get(section, key, strNull);
}
