#ifndef SCENE_H
#define SCENE_H

class Map;
class SceneView;

class Scene
{
public:
    Scene();
    ~Scene();

    void LoadMap(const char* filename);
    void SaveMap(const char* filename);

    void GenerateMap();

    void PrintMap();

    void Draw() const;

    const Map* GetMap() const { return m_map; }
private:
    Map *m_map;
};

#endif // SCENE_H
