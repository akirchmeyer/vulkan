#include "map.h"
#include <memory>
#include <cassert>
#include <iostream>
#include <iomanip>

Map::Map() : m_width(0), m_height(0), m_cells(nullptr)
{
}

Map::~Map()
{
    if(m_cells)
    {
        delete m_cells;
        m_cells = nullptr;
    }
}

void Map::Print() const
{
    std::cout << "\n\nPrinting Map: \n";

        for(uint32 y = 0; y < m_height; ++y)
        {
            for(uint32 x = 0; x < m_width; ++x)
            {
                std::cout << std::setw(2) << (int)m_cells[y*m_width + x] << ' ';
            }
            std::cout << '\n';
        }
        std::cout << '\n';
}

void Map::Construct(const hsize *data, uint32 width, uint32 height)
{
    m_width = width;
    m_height = height;

    m_cells = new hsize[width*height];
    std::copy(data, data + (width*height), m_cells);

    assert(m_cells);

}
